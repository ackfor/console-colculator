﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using ConsoleCalculator;

namespace CalcTesting
{
    [TestClass]
    public class TestMathExpressionSplitter
    {
        private MathExpressionSplitter Splitter;

        public TestMathExpressionSplitter()
        {
            var settings = new CalculatorSettings();

            Splitter = new MathExpressionSplitter(settings);
        }

        [TestMethod]
        public void EasyTest()
        {
            var expected =
                new string[] { "(", "8", "+", "2", "*", "5", ")", "/", "(", "1", "+", "3", "*", "2", "-", "4", ")" };

            var mathExprassion = "(8+2*5)/(1+3*2-4)";
            var actual = Splitter.SplitMathExprassion(mathExprassion);

            Assert.AreEqual(expected.Length, actual.Length);

            for (int i = 0; i < expected.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i]);
            }
        }

        [TestMethod]
        public void TestWithPrefFunc()
        {
            var expected =
                new string[] { "-", "(", "8", "+", "2", "*", "5", ")", "/", "(", "1", "+", "3", "*", "2", "-", "4", ")" };

            var mathExprassion = "-(8+2*5)/(1+3*2-4)";
            var actual = Splitter.SplitMathExprassion(mathExprassion);

            Assert.AreEqual(expected.Length, actual.Length);

            for (int i = 0; i < expected.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i]);
            }
        }

    }

    [TestClass]
    public class TestReversePolishNotation
    {
        private ReversePolishNotation RPN;

        public TestReversePolishNotation()
        {
            var settings = new CalculatorSettings();

            RPN = new ReversePolishNotation(settings);
        }

        [TestMethod]
        public void EasyTest()
        {
            var expected = 
                new Queue<string>(new string[] { "8", "2", "5", "*", "+", "1", "3", "2", "*", "+", "4", "-", "/" });

            var infixNotation = 
                new string[] { "(", "8", "+", "2", "*", "5", ")", "/", "(", "1", "+", "3", "*", "2", "-", "4", ")" };

            foreach (var member in RPN.InfixToRPN(infixNotation))
            {
                Assert.AreEqual(expected.Dequeue(), member);
            }
        }

        [TestMethod]
        public void TestWithPrefFunc()
        {
            var expected =
                new Queue<string>(new string[] { "8", "2", "5", "*", "+", "~", "1", "3", "2", "*", "+", "4", "-", "/" });

            var infixNotation =
                new string[] { "~", "(", "8", "+", "2", "*", "5", ")", "/", "(", "1", "+", "3", "*", "2", "-", "4", ")" };

            foreach (var member in RPN.InfixToRPN(infixNotation))
            {
                Assert.AreEqual(expected.Dequeue(), member);
            }
        }
    }

    [TestClass]
    public class TestCulculator
    {
        private Calculator calculator;
        public TestCulculator()
        {
            calculator = new Calculator();
        }

        [TestMethod]
        public void EasyPlusTest()
        {
            Assert.AreEqual(10.0, calculator.Calculate("3+7"));
        }

        [TestMethod]
        public void EasyMinusTest()
        {
            Assert.AreEqual(-4.0, calculator.Calculate("3-7"));
        }

        [TestMethod]
        public void EasyMultiplicationTest()
        {
            Assert.AreEqual(21.0, calculator.Calculate("3*7"));
        }

        [TestMethod]
        public void EasyDivisionTest()
        {
            Assert.AreEqual(3.0 / 7, calculator.Calculate("3/7"));
        }

        [TestMethod]
        public void PlusAndSpaceTest()
        {
            Assert.AreEqual(10.0, calculator.Calculate("   3    +    7"));
        }

        [TestMethod]
        public void EasyPlusWithDotsTest()
        {
            Assert.AreEqual(10.0, calculator.Calculate("3,0+7"));
        }

        [TestMethod]
        public void ToEasyTest()
        {
            Assert.AreEqual(6.0, calculator.Calculate("6,0"));
        }

        [TestMethod]
        public void NormalTest()
        {
            Assert.AreEqual(6.0, calculator.Calculate("(8+2*5)/(1+3*2-4)"));
        }

        [TestMethod]
        public void TestWithPrefFunc()
        {
            Assert.AreEqual(-6.0, calculator.Calculate("-(8+2*5)/(1+3*2-4)"));
        }

        [TestMethod]
        public void TestWithPrefFunc2()
        {
            Assert.AreEqual(-6.0, calculator.Calculate("(8+2*5)/-(1+3*2-4)"));
        }

        [TestMethod]
        public void TestWithPrefFunc3()
        {
            Assert.AreEqual(2.0/3, calculator.Calculate("(-8+2*5)/(1+3*2-4)"));
        }

        [TestMethod]
        public void TestWithPrefFunc4()
        {
            Assert.AreEqual(6.0, calculator.Calculate("(+8+2*5)/(1+3*2-4)"));
        }

        [TestMethod]
        public void NormalTestWithSpace()
        {
            Assert.AreEqual(6.0, calculator.Calculate("(8+2*5)    /    (1+3*2-4)"));
        }
    }

    [TestClass]
    public class CorrectExprassionTest
    {
        private Calculator calculator;
        public CorrectExprassionTest()
        {
            calculator = new Calculator();
        }

        [TestMethod]
        public void EasyTest1()
        {
            Assert.AreEqual(true, calculator.IsExprassionCorrect(
                new string[] { "-","8","/","-","2","+","3" }));
        }

        [TestMethod]
        public void EasyTest2()
        {
            Assert.AreEqual(true, calculator.IsExprassionCorrect(
                new string[] { "8", "/", "-", "2", "+", "3" }));
        }

        [TestMethod]
        public void EasyTest3()
        {
            try
            {
                calculator.IsExprassionCorrect(
                    new string[] { "8", "/", "*", "2", "+", "3" });

                throw new AssertFailedException();
            }
            catch (Exception e)
            {
                Assert.AreEqual("Expected prefix function '+' or '-'", e.Message);
            }
        }

        [TestMethod]
        public void EasyTest4()
        {
            try
            {
                calculator.IsExprassionCorrect(
                    new string[] { "8", "/", "-", "2", "+", "3", "3" });

                throw new AssertFailedException();
            }
            catch (Exception e)
            {
                Assert.AreEqual("Expected prefix function '+' or '-'", e.Message);
            }
        }

        [TestMethod]
        public void EasyTest5()
        {
            try
            {
                calculator.IsExprassionCorrect(
                    new string[] { "8", "/", "-", "2", "+", "3", "3", "3" });

                throw new AssertFailedException();
            }
            catch (Exception e)
            {
                Assert.AreEqual("Has no operation", e.Message);
            }
        }
    }

    [TestClass]
    public class Experiments
    {
        [TestMethod]
        public void OperationTest()
        {
            Assert.AreEqual(0.0, 5+-5);
        }
    }
}
