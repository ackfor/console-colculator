﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleCalculator
{
    /// <summary>
    ///     Содержит всю информацию о калькуляторе.
    /// </summary>
    public class CalculatorSettings
    {
        public readonly int OperationsCount;
        public readonly int OpenBracketsCount;
        public readonly int CloseBracketsCount;

        private readonly string[] Operations;
        private char[] OpenBrackets;
        private char[] CloseBrackets;
        public readonly char[] Dots;
        private Dictionary<string, Func<double, double, double>> BinFunctions;
        private Dictionary<string, Func<double, double>> UnaryFunc;

        public CalculatorSettings()
        {
            Operations = new string[] { "-", "+", "/", "*", "~", "p" }; //От расположения будет зависить приоритет
            OperationsCount = Operations.Length;

            OpenBrackets = new char[] { '(', '[', '{' };
            OpenBracketsCount = OpenBrackets.Length;

            CloseBrackets = new char[] { ')', ']', '}' };
            CloseBracketsCount = CloseBrackets.Length;

            Dots = new char[] { ',' };

            BinFunctions = new Dictionary<string, Func<double, double, double>>();
            BinFunctions.Add("+", (x, y) => x + y);
            BinFunctions.Add("-", (x, y) => x - y);
            BinFunctions.Add("*", (x, y) => x * y);
            BinFunctions.Add("/", (x, y) => x / y);

            UnaryFunc = new Dictionary<string, Func<double, double>>();
            UnaryFunc.Add("~", (x) => -1 * x);
            UnaryFunc.Add("p", (x) => x);
        }

        public bool IsOperation(string str)
        {
            return Operations.Contains(str);
        }

        public bool IsUnaryFunc(string str)
        {
            return 
                str == "~" ||
                str == "p";
        }

        public bool IsBinFunc(string str)
        {
            return !IsUnaryFunc(str);
        }

        public bool IsOpenBracket(string str)
        {
            return str.Length == 1 && OpenBrackets.Contains(str[0]);
        }

        public bool IsCloseBracket(string str)
        {
            return str.Length == 1 && CloseBrackets.Contains(str[0]);
        }

        /// <summary>
        ///     Возвращает все, что может содержаться во входном выражении, кроме чисел.
        /// </summary>
        /// <returns>
        ///     Возвращает ленивую колекцию. 
        /// </returns>
        public IEnumerable<string> GetFuncMembers()
        {
            foreach (var member in Operations)
            {
                yield return member;
            }

            foreach (var member in OpenBrackets)
            {
                yield return member.ToString();
            }

            foreach (var member in CloseBrackets)
            {
                yield return member.ToString();
            }
        }

        public Func<double, double, double> GetBinFunc(string operation)
        {
            if (!Operations.Contains(operation))
                throw new Exception(string.Format("Unknown operation\"{0}\"", operation));

            return BinFunctions[operation];
        }

        public Func<double, double> GetUnaryFunc(string operation)
        {
            if (!Operations.Contains(operation))
                throw new Exception(string.Format("Unknown operation\"{0}\"", operation));

            return UnaryFunc[operation];
        }

        public int GetPriority(string operation)
        {
            switch (operation)
            {
                case "p": return 5;
                case "~": return 5;
                case "*": return 4; //Сознательно хочу, чтобы умножалось раньше чем делилось.
                case "/": return 3;
                case "+": return 2; //Сознательно хочу, чтобы складывалось раньше чем делилось.
                case "-": return 1;

                default:
                    break;
            }

            throw new Exception("Not operation was given");
        }

        public bool IsBracketsPair(string ch1, string ch2)
        {
            return
                ch1 == "(" && ch2 == ")" ||
                ch1 == ")" && ch2 == "(" ||
                ch1 == "[" && ch2 == "]" ||
                ch1 == "]" && ch2 == "[" ||
                ch1 == "{" && ch2 == "}" ||
                ch1 == "}" && ch2 == "{";

        }

        //Унарная в смысле, что её можно подставить перед числом пр. +8 ; -7.81
        public bool CanBePrefixFunc(string operation)
        {
            return
                operation == "+" ||
                operation == "-";
        }

        public bool CanBeBinFunc(string operation)
        {
            return 
                operation == "+" ||
                operation == "-" ||
                operation == "*" ||
                operation == "/";
        }

        public void GetSimbosPrefixFunc(ref string operation)
        {
            if (operation == "+")
            {
                operation = "p";
                return;
            }

            if (operation == "-")
            {
                operation = "~";
                return;
            }

            throw new Exception(string.Format("({0}) not prefix function", operation));
        }
    }
}
