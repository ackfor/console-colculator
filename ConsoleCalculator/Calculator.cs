﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleCalculator
{
    /// <summary>
    /// Калькуляторп, который производит вычисления из введенного выражения
    /// </summary>
    public class Calculator
    {
        private CalculatorSettings Settings;
        private Stack<double> Stack;
        MathExpressionSplitter Splitter;
        ReversePolishNotation RPNotation;

        public Calculator()
        {
            Settings = new CalculatorSettings();
            Stack = new Stack<double>();
            Splitter = new MathExpressionSplitter(Settings);
            RPNotation = new ReversePolishNotation(Settings);
        }

        /// <summary>
        ///     Основной метод в классе. Занимается вычислениями,
        ///     и проверками на правильность ввода.
        /// </summary>
        /// <param name="mathExpression">
        ///     Введенное математическое выражение
        /// </param>
        /// <returns>
        ///     Результат расчетов, если в конце подсчетов, в стеке осталось только одно число.
        /// </returns>
        /// <exception cref="System.Exception">
        ///     Exception, если в итоге работы программы в стеке осталось что-то,
        ///     кроме ответа. Это значит, что в веденном выражении ошибка
        /// </exception>
        public double Calculate(string mathExpression)
        {
            var members = Splitter.SplitMathExprassion(mathExpression);

            StartCheck(members);

            PrefOperationToFunc(members);

            return MainCulc(members);
        }

        /// <summary>
        ///     Выполнение проверок перед выполнением рассчетов.
        /// </summary>
        /// <param name="members">
        ///     Поделенное на составляющие входное выражение 
        /// </param>
        private void StartCheck(string[] members)
        {
            if (!IsBracketSequenceCorrect(members))
                throw new Exception("Not correct bracket sequence");

            try
            {
                // Использую IsCorrectNumber раз уж сделал
                members.All(str =>
                    Settings.GetFuncMembers().Contains(str) || IsCorrectNumber(str));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        ///     Заменяет + на "p" и - на "~", которые стоят как знаки отрицания пр: -(), +9.
        /// </summary>
        /// <remarks>
        ///     Ее работа построена на предположении, что пользователь ввел правильное выражение.
        ///     Это нужно для упрощения поиска ошибок и конечных вычислений результата.
        /// </remarks>
        /// <param name="members">
        ///     Поделенное на составляющие входное выражение 
        /// </param>
        private void PrefOperationToFunc(string[] members)
        {
            if (members.Length < 2)
                return;

            if (Settings.CanBePrefixFunc(members[0]))
            {
                Settings.GetSimbosPrefixFunc(ref members[0]);
                return;
            }

            for (int i = 1; i < members.Length - 1; i++) //i можно вообще со 2-го индекса начать
            {
                if (Settings.CanBePrefixFunc(members[i]))
                {
                    if ((Settings.IsOpenBracket(members[i + 1]) || char.IsDigit(members[i + 1][0])) &&
                        (Settings.IsOpenBracket(members[i - 1]) || Settings.CanBeBinFunc(members[i - 1])))
                    {
                        Settings.GetSimbosPrefixFunc(ref members[i]);
                    }
                }
            }
        }

        /// <summary>
        ///     Производит основные вычисления.
        /// </summary>
        /// <param name="members">
        ///     Поделенное на составляющие входное выражение.
        /// </param>
        /// <returns>
        ///     Посчитанный результат.
        /// </returns>
        private double MainCulc(string[] members)
        {
            foreach (var member in RPNotation.InfixToRPN(members))
            {
                if (!Settings.IsOperation(member))
                {
                    var result = 0.0;
                    try
                    {
                        double.TryParse(member, out result);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    Stack.Push(result);
                    continue;
                }
                else if (Settings.IsUnaryFunc(member))
                {
                    var operand = Stack.Pop();

                    Stack.Push(
                        Settings.GetUnaryFunc(member)(operand));

                    continue;
                }
                var secondOperand = Stack.Pop();
                var firstOperand = Stack.Pop();

                Stack.Push(
                    Settings.GetBinFunc(member)(firstOperand, secondOperand));
            }
            if (Stack.Count != 1) throw new Exception("Not correct expression");

            return Stack.Pop();
        }

        /// <summary>
        ///     Проверяет правильность расставления скобок в выражении.
        /// </summary>
        /// <param name="bracketsequence">
        ///     В контексте этой программы, это введенная строка.
        /// </param>
        /// <returns>
        ///     true - tследовательность правильная, иначе false
        /// </returns>
        private bool IsBracketSequenceCorrect(string[] bracketsequence)
        {
            var stack = new Stack<string>();

            foreach (var str in bracketsequence)
            {
                if (Settings.IsOpenBracket(str))
                {
                    stack.Push(str);
                }

                if (Settings.IsCloseBracket(str))
                {
                    if (!Settings.IsBracketsPair(stack.Pop(), str))
                    {
                        return false;
                    }
                }
            }
            return stack.Count == 0;
        }

        /// <summary>
        ///     Проверяет, является ли строка положительным числом типа double.
        ///     Сделано для конкретизации ошибок.
        /// </summary>
        /// <param name="number">
        ///     Строка кондидат
        /// </param>
        /// <returns>
        ///     true - является числом типа double, иначе flase
        /// </returns>
        /// <exception cref="Exception">
        ///     В ошибке содержится пояснение, что конкретно метод считает неправильным.
        /// </exception>
        private bool IsCorrectNumber(string number)
        {
            bool dotExist = false;

            if (number[0] == Settings.Dots[0]) 
                throw new Exception(string.Format("\"{0}\" can't start with dot", number));

            if (number[number.Length - 1] == Settings.Dots[0])
                throw new Exception(string.Format("\"{0}\" can't end with dot", number));

            for (int i = 0; i < number.Length; i++)
            {
                if(number[i] == Settings.Dots[0])
                {
                    if (dotExist) 
                        throw new Exception(string.Format("\"{0}\" have so much dots", number));

                    dotExist = true;
                    continue;
                }

                if (number[i] == ' ') throw new Exception(string.Format("\"{0}\" Unexpected space", number));

                if (!char.IsDigit(number[i])) throw new Exception("\"{0}\" Unexpected symbols in number");
            }
            return true;
        }

        /// <summary>
        ///     Нужно для проверки правильности расставления операций.
        /// </summary>
        /// <remarks>
        ///     Не может реагировать нормально на собки.
        ///     Нуджна, чтобы конкретизировать ошибки.
        ///     ИСПОЛЬЗУЕТСЯ ТОЛЬКО В ТЕСТАХ.
        ///     В ОСНОВНОМ КОДЕ НЕ ИСПОЛЬЗУЕТСЯ.
        /// </remarks>
        /// <param name="members">
        ///     Поделенное на составляющие входное выражение
        /// </param>
        /// <returns>
        ///     true - если выражение верно, иначе false
        /// </returns>
        public bool IsExprassionCorrect(string[] members)
        {
            return CheckSubExprsn(members, 0, members.Length-1);
        }

        private bool CheckSubExprsn(string[] members, int startIndex, int endIndex)
        {
            if (endIndex - startIndex < 2)
            {
                return IsActionCorrect(members, startIndex, endIndex);
            }

            var splitIndex = GetBinaryOperationIndex(members, startIndex, endIndex);

            return CheckSubExprsn(members, startIndex, splitIndex - 1) &&
                        CheckSubExprsn(members, splitIndex + 1, endIndex);
        }

        private bool IsActionCorrect(string[] members, int startIndex, int endIndex)
        {
            if (startIndex > endIndex)
                throw new Exception("Not correct exprassion");

            if (endIndex == startIndex)
            {
                if (char.IsDigit(members[startIndex][0]))
                    return true;
                throw new Exception("Not correct exprassion");
            }

            if (endIndex - startIndex == 1)
            {
                if (Settings.IsOpenBracket(members[startIndex]) &&
                    Settings.IsOpenBracket(members[endIndex]))
                    return true;

                if (!Settings.CanBePrefixFunc(members[startIndex]))
                    throw new Exception("Expected prefix function '+' or '-'");

                if (!char.IsDigit(members[endIndex][0]))
                    throw new Exception("Expected number");

                members[endIndex] = (members[endIndex] == "-") ? "~" : "";

                return true;
            }
            throw new Exception("Unexpected action");
        }


        private int GetBinaryOperationIndex(string[] members, int startIndex, int endIndex)
        {
            int i = startIndex + 1;

            if (members[startIndex] == "*" ||
                members[startIndex] == "/")
                throw new Exception("Expected prefix function '+' or '-'");

            for (; i < members.Length; i++)
            {
                if (char.IsDigit(members[i][0])) continue;

                if (Settings.GetFuncMembers().Contains(members[i]))
                {
                    return i;
                }
            }

            throw new Exception("Has no operation");
        } 
    }
}