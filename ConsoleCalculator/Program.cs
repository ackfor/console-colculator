﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            var calculator = new Calculator();

            var mathExpression = Console.ReadLine();

            var result = 0.0;
            try
            {
                result = calculator.Calculate(mathExpression);
            }
            catch (Exception e)
            {
                Console.WriteLine("\nExeption: {0}", e.Message);
            }

            Console.WriteLine("/nIt equal {1}", result);
        }
    }
}
