﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleCalculator
{
    /// <summary>
    ///     Класс в котором содержатся методы для разделения ввходной последовательности на логические части
    /// </summary>
    public class MathExpressionSplitter
    {
        private CalculatorSettings Settings;

        public MathExpressionSplitter(CalculatorSettings settings)
        {
            Settings = settings;
        }

        /// <summary>
        ///     Функция, которая осуществляет разделение входной строки на логические части.
        /// </summary>
        /// <param name="exprassion">
        ///     Входная строка
        /// </param>
        /// <remarks>
        ///     ДЛя простоты использую, что все разделители состоят из одного числа.
        /// </remarks>
        /// <returns>
        ///     Разделенная на логические части входная строка. Без лишних пробелов
        /// </returns>
        public string[] SplitMathExprassion(string exprassion)
        {
            var list = new List<string>();
            var tmp = new StringBuilder();

            for (int i = 0; i < exprassion.Length; i++)
            {
                tmp.Append(exprassion[i]);

                if (char.IsDigit(exprassion[i]) || exprassion[i] == ' ' || exprassion[i] == Settings.Dots[0])
                    continue;

                foreach (var sepurator in Settings.GetFuncMembers())
                {
                    if (sepurator[0] == exprassion[i])
                    {
                        if (tmp.Length != 1)
                        {
                            tmp.Remove(tmp.Length - 1, 1); //удаляю символ разделителя
                            if (!isSpaceOnly(tmp))
                            {
                                RemoveAroundSpace(tmp);
                                list.Add(tmp.ToString());
                            }
                            
                        }
                        list.Add(sepurator);
                        tmp.Clear();
                        break;
                    }
                }
            }
            if(tmp.Length != 0)
            {
                RemoveAroundSpace(tmp);
                list.Add(tmp.ToString());
            }
            return list.ToArray();
        }

        /// <summary>
        ///     Удаляет ненужные пробеллы вокруг числа. 
        /// </summary>
        /// <remarks>
        ///     Gjlhfpevtdftncz
        /// </remarks>
        /// <param name="builder">
        ///     Строка, гарантированно состоящая не только из пробелов
        /// </param>
        private void RemoveAroundSpace(StringBuilder builder)
        {
            if (builder.Length == 0)
                return;

            while (builder[builder.Length -1] == ' ')
            {
                builder.Remove(builder.Length - 1, 1);
            }

            while (builder[0] == ' ')
            {
                builder.Remove(0, 1);
            }
        }

        /// <summary>
        ///     Проверяет наличия символов, отличных от пробела
        /// </summary>
        /// <param name="builder">
        ///     Строка, которая может состоять из символов пробела
        /// </param>
        /// <returns>
        ///     true - отличные от пробела символы есть, иначе false
        /// </returns>
        private bool isSpaceOnly(StringBuilder builder)
        {
            for (int i = 0; i < builder.Length; i++)
            {
                if (builder[i] != ' ') return false;
            }
            return true;
        }
    }
}
