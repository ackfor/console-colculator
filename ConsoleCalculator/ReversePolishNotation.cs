﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleCalculator
{
    /// <summary>
    ///     Класс в котором содержаться методы для преобразования инфиксной записи
    ///     в постфиксную (обратную польскую нотацию)
    /// </summary>
    public class ReversePolishNotation
    {
        private CalculatorSettings OperationSettings;

        public ReversePolishNotation(CalculatorSettings operationSettings)
        {
            OperationSettings = operationSettings;
        }

        /// <summary>
        ///     Основной метод, который преобразует инфиксную в постфиксную нотации
        /// </summary>
        /// <param name="infixNotation">
        ///     Ленивая коллекция, содержащая части инфикснового выражения
        /// </param>
        /// <returns>
        ///     Ленивая коллекция, содержащая части поствиксного выражения
        /// </returns>
        public IEnumerable<string> InfixToRPN(IEnumerable<string> infixNotation)
        {
            var stack = new Stack<string>();
            string popResult = "";

            foreach (var member in infixNotation)
            {
                if (OperationSettings.IsOpenBracket(member))
                {
                    stack.Push(member);
                    continue;
                }

                if (OperationSettings.IsCloseBracket(member))
                {
                    popResult = stack.Pop();
                    while (!OperationSettings.IsOpenBracket(popResult))
                    {
                        yield return popResult;
                        popResult = stack.Pop();
                    }
                    continue;
                }

                if (OperationSettings.IsOperation(member))
                {
                    while(true)
                    {
                        if (stack.Count == 0)
                            break;

                        popResult = stack.Peek();

                        if (!(OperationSettings.IsOperation(popResult) &&
                            ComparePriority(popResult, member) > 0))
                            break;

                        yield return stack.Pop();
                    }
                    stack.Push(member);
                    continue;
                }
                yield return member;
            }
            while (stack.Count != 0) yield return stack.Pop();
        }

        /// <summary>
        ///     Сравнивает приоритеты друх операцийы
        /// </summary>
        /// <param name="operation1">Первый операнд</param>
        /// <param name="operation2">Второй операнд</param>
        /// <returns></returns>
        private int ComparePriority(
            string operation1, string operation2)
        {
            return 
                OperationSettings.GetPriority(operation1) - OperationSettings.GetPriority(operation2);
        }
    }
}
